# django_docker_jenkins

This is a ready-to-use boiler plate repo for a containerised CI driven web application.

##Usage:
1. Install docker and docker-compose on your system
2. Execute the command "docker-compose up"

Voila! You have a containerised PythonDjango Web Application backed by a Postgres database, and Jenkins CI server to automate your building and testing workflows!

Feedback and PRs are most welcome :)

Medium Post: https://bit.ly/2jzTikl